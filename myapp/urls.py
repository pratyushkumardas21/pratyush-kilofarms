from django.urls import path
from . import views


from django.conf.urls import url,include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views

router = routers.DefaultRouter()
router.register(r'signup', views.UserViewSet)

urlpatterns=[

    path('login/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^', include(router.urls)),

    path('product/<str:pk>/', views.product, name="product-get-post-delete"),
    path('createSKU/', views.productCreate, name="createSKU?"),
]