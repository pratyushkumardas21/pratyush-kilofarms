from django.shortcuts import render

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import products,User
from .serializers import productSerializer,UserSerializer
from rest_framework.parsers import JSONParser

from rest_framework import viewsets
from django.http.response import JsonResponse
# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


@api_view(["POST"])
def productCreate(request):
    serializer = productSerializer(data = request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(['GET', 'PUT','DELETE'])
def product(request,pk):
    if request.method == 'GET':
        lists = products.objects.get(id=pk)
        serializer = productSerializer(lists, many=False)
        return Response(serializer.data)

    elif request.method == 'PUT':
        lists = products.objects.get(id=pk)
        product_data = JSONParser().parse(request)
        serializer = productSerializer(lists, data=product_data)

        if serializer.is_valid():
            serializer.save()
        return JsonResponse(serializer.data)

    elif request.method == 'DELETE':
        product_delete = products.objects.get(id=pk)
        product_delete.delete()
        return Response("DELETE SUCCESS")
