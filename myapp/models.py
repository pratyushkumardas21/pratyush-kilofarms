from django.db import models

from django.contrib.auth.models import AbstractUser
from django.conf import settings
# Create your models here.


class User(AbstractUser):
    username = models.CharField(max_length=20,blank=True, null=True)
    email = models.EmailField(('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return "{}".format(self.email)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    phoneNumber = models.IntegerField(default=0)
    DOB = models.DateField()


class products(models.Model):
    id = models.AutoField(primary_key=True)
    sku_name = models.CharField(max_length=50,default='')
    SKU_CATEGORY = [('Fruits', 'Fruits'),('Vegetables', 'Vegetables'),('Dry Fruits', 'Dry Fruits'),('Others', 'Others')]
    sku_category = models.CharField(max_length=20, choices=SKU_CATEGORY, default='pending')
    price = models.FloatField()

